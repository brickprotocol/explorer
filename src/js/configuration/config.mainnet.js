const nodeUrl = 'https://node1.brickprotocol.com';

export default {
    networkId: 'mainnet',
    displayName: 'Mainnet',
    apiBaseUrl: nodeUrl,
    nodes: [{url: nodeUrl, maintainer: 'Brickwise Technologies'},{url: 'https://node2.brickprotocol.com', maintainer: 'Brickwise Technologies'}]
};
