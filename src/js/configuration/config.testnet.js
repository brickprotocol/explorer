const nodeUrl = 'https://testnode1.brickprotocol.com';

export default {
    networkId: 'testnet',
    displayName: 'Testnet',
    apiBaseUrl: nodeUrl,
    nodes: [
        {url: nodeUrl, maintainer: 'Brickwise Technologies', showAsLink: true},
        {url: 'https://testnode2.brickprotocol.com', maintainer: 'Brickwise Technologies', showAsLink: true}
    ],
    /*faucet: {
        url: 'https://testnode1.wavesnodes.com/faucet',
        captchaKey: '6LdT8pAUAAAAAOhIIJGKA6HAOo7O98gdIoUgznKL',
        address: '3Myqjf1D44wR8Vko4Tr5CwSzRNo2Vg9S7u7'
    }*/
};
